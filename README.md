# Capture thermal and visible picture and apply transmorphing algorithm


## Requirement:
* Compile transmorphing in file JPEG_transmorph_code
* Python libary
    * PiCamera: https://picamera.readthedocs.io/en/release-1.13/
	* MLX9064x: https://github.com/melexis-fir/mlx9064x-driver-py 

## Usage:
To launch the function
`python3 allStep.py <images_file_path> <key> <numberIteration>`
