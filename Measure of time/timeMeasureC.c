/* time measure program*/
    #include <stdio.h>
    #include <time.h>
    
    int main(){
        clock_t t0, t1;
        double deltaT;
        
        t0 = clock();
        /* ...
        function that must be measured in time 
        ... */
        t1 = clock();
        deltaT = ((double)(t1-t0))/ CLOCKS_PER_SEC;
        printf('deltaT is: %f \n', deltaT)
        return 0;
    }