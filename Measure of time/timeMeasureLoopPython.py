# time measure program for 100 passes in a loop
import time
import numpy as np
    
t0 = 0
t1 = 0

nbr = 100
deltaT = np.zeros(nbr)

for i in range(0, nbr):

	t0 = time.time_ns()
	# ...
	# function that must be measured in time 
	# ...
	t1 = time.time_ns()

	deltaT[i] = t1-t0
print("Mean deltaT" + str(np.mean(deltaT)) + " ns and " + str(np.mean(deltaT)/10**9) + " s")