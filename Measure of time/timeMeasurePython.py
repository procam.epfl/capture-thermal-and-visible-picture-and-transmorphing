# time measure program
import time
    
t0 = 0
t1 = 0

t0 = time.time_ns()
# ...
# function that must be measured in time 
# ...
t1 = time.time_ns()

deltaT = t1-t0
print("deltaT" + str(deltaT) + " ns and " + str(deltaT/10**9) + " s")