##
CAMERA_ID=1 #camera id

JPTRSMORPH_BIN=/home/pi/Desktop/JPEG_transmorph_code/jptrsmorph 
										#path to the jptrsmorph binary
VISIBLE_OUTPUT=/home/pi/Desktop/build/visible.jpeg
IR_OUTPUT=/home/pi/Desktop/build/thermal.jpeg
BUILD_DIR=/home/pi/Desktop/build

python3 /home/pi/Desktop/module/module1_create_visible_jpeg.py $VISIBLE_OUTPUT 
									#path to the visibe camera module
python3 /home/pi/Desktop/module/module2_create_ir_jpeg.py $IR_OUTPUT;
									#path to the thermal camera module
current_time=$(date "+%Y.%m.%d-%H.%M.%S");
file_name=$BUILD_DIR/$CAMERA_ID-$current_time.jpeg;
$JPTRSMORPH_BIN -morph $VISIBLE_OUTPUT $IR_OUTPUT $file_name;
