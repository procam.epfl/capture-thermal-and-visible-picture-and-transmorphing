import sys
from subprocess import call
from picamera import PiCamera

from os import path
import numpy as np
from PIL import Image
import board
import busio
import adafruit_mlx90640

from matplotlib import cm

resolution = (1024, 768)
Tmin = 0
Tmax = 60

def init(camera):
    camera.resolution = resolution
    
# function to convert temperatures to pixels on image
def td_to_image(f, Tmin, Tmax):
    norm = [(scale(v,Tmin,Tmax)-Tmin)/(Tmax-Tmin) for v in f]
    norm = cm.inferno(norm)*255
    norm = np.uint8(norm)
    norm.shape = (24,32,4)
    return norm

def scale(val, min, max):
    if val < min:
        return min
    elif val > max:
        return max
    else:
        return val

def make_image(frame,  output_file, Tmin , Tmax):
    ta_img = td_to_image(frame, Tmin, Tmax)
    img = Image.fromarray(ta_img)
    img = img.convert('RGB')
    img.save(output_file)
    
def loop(camera, mlx, visible_file, ir_file, out_path, key):
        t0 = time.time_ns()
        camera.capture(visible_file)
        t1 = time.time_ns()

        frame = [0] * 768
        mlx.getFrame(frame)
        make_image(frame, ir_file, Tmin, Tmax)

        t2 = time.time_ns()
        
        call(["./JPEG_transmorph_code/jptrsmorph","-morph", "-key", key, visible_file, ir_file, out_path ])
        t3 = time.time_ns()

        print("visible image:" + str(t1-t0) + " ns and " + str((t1-t0))/10**9) + " s")
        print("thermal image:" + str(t2-t1) + " ns and " + str((t2-t1)/10**9) + " s")
        print("transmorphing:" + str(t3-t2) + " ns and " + str((t3-t2)/10**9) + " s")
        
    
def main():
    t0init = time.time_ns()
    camera = PiCamera()
    init(camera)
    t1init = time.time_ns()
    print("Visible Cam. init Time:" + str(t1init-t0init) + " ns and " + str((t1init-t0init)/10**9) + " s")
    
    t0init = time.time_ns()
    i2c = busio.I2C(board.SCL, board.SDA, frequency=200000)
    mlx = adafruit_mlx90640.MLX90640(i2c)
    mlx.refresh_rate = adafruit_mlx90640.RefreshRate.REFRESH_2_HZ
    t1init = time.time_ns()
    print("Thermal Cam. init Time:" + str(t1init-t0init) + " ns and " + str((t1init-t0init)/10**9) + " s")

    loop(camera, mlx, sys.argv[1] + 'visible.jpeg', sys.argv[1] + 'thermal.jpeg', sys.argv[1] + 'out.jpeg', sys.argv[2])

if __name__ == "__main__":
    main()


    
