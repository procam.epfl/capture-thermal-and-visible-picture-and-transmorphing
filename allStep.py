#Program to apture thermal and visible image
#and to apply transmorphing
#
#To launch module
#python3 allStep.py images_file_path key numberIteration

#library for the visible Camera
from picamera import PiCamera
#library for thermal camera
#---
#To instal:
#pip3 install mlx9064x-driver
#https://github.com/melexis-fir/mlx9064x-driver-py
import mlx.mlx90640 as mlx
#library for array and matrix
import numpy as np
#library for recovering the arguments of the command terminal
import sys
#library to launch command terminal
from subprocess import call
#library to transform temp. matrix in RGB matrix
from matplotlib import cm
#library to generate image from matrix
from PIL import Image
#library to measure time program
import time

#constant in program
#resolution of visible camera
resolution=(1024, 768)
#temperature range for thermal camera
Tmin = 10
Tmax = 50
#framerate for visible and thermal camera
framerate = 64
#path to the transmorph executable
transmorph_path = "JPEG_transmorph_code"

#function to initialize the visible camera
def init(camera):
    camera.resolution = resolution
    camera.framerate = framerate
    

# function to convert temperatures to pixels on image
def td_to_image(f, Tmin, Tmax):
    norm = [(scale(v,Tmin,Tmax)-Tmin)/(Tmax-Tmin) for v in f]
    norm = cm.inferno(norm)*255
    norm = np.uint8(norm)
    norm.shape = (24,32,4)
    return norm

# function to change temperature out of range
def scale(val, min, max):
    if val < min:
        return min
    elif val > max:
        return max
    else:
        return val

# function to transforme a temp. matrix to an image
def make_image(frame,  output_file, Tmin , Tmax):
    ta_img = td_to_image(frame, Tmin, Tmax)
    img = Image.fromarray(ta_img)
    img = img.convert('RGB')
    img.save(output_file)
    
def loop(camera, ir, nbr, visible_file, ir_file, out_path, key):
    #initilise time variable
    t0 = 0
    t1 = 0
    t2 = time.time_ns()
    tcam = np.zeros(nbr)
    tir = np.zeros(nbr)
    tmorph = np.zeros(nbr)
    
    #take visible picture and launch the loop
    for i, filename in enumerate(camera.capture_continuous(visible_file)):
        t0 = time.time_ns()
        tcam[i] = t0-t2
        print("image:" + str(tcam[i]) + " ns and " + str(tcam[i]/10**9) + " s")
        #take temperature matrix with thermal camera
        frame = ir.read_frame()
        f = ir.do_compensation(frame)
        #transform temperature matrix to an image
        make_image(f, ir_file, Tmin, Tmax)
        #create path file for the transmorphed image
        out = out_path + time.strftime('_%d-%m-%Y_%H:%M:%S',time.localtime()) + '.jpeg'
        t1 = time.time_ns()
        #launch transmorphin Algorithm
        call(["./"+transmorph_path+"/jptrsmorph","-morph", "-key", key, visible_file, ir_file, out ])
        t2 = time.time_ns()
        tir = t1-t0
        tmorph = t2-t1
        print("ir:" + str(tir) + " ns and " + str(tir/10**9) + " s")
        print("morphing:" + str(tmorph) + " ns and " + str(tmorph/10**9) + " s")
        if i == (nbr-1):
            break
    print('<<<<----------------------->>>>')
    print("image:" + str(np.mean(tcam)) + " ns and " + str(np.mean(tcam)/10**9) + " s")
    print("ir:" + str(np.mean(tir)) + " ns and " + str(np.mean(tir)/10**9) + " s")
    print("morphing:" + str(np.mean(tmorph)) + " ns and " + str(np.mean(tmorph)/10**9) + " s")
    
def main():
    t0init = time.time_ns()
    #initialize thermal camera
    camera = PiCamera()
    init(camera)
    #initialize thermal camera
    ir = mlx.Mlx9064x('I2CBB-03-05', i2c_addr=0x33, frame_rate=framerate)                                                                                                                 
    ir.init()
    t1init = time.time_ns()
    print("Init Time:" + str(t1init-t0init) + " ns and " + str((t1init-t0init)/10**9) + " s")
    #do loop to capture image and to transmorph
    loop(camera, ir, int(sys.argv[3]), sys.argv[1] + 'visible.jpeg', sys.argv[1] + 'thermal.jpeg', sys.argv[1] + 'out', sys.argv[2])

if __name__ == "__main__":
    main()


    
